import logging

image_logger = logging.getLogger('image_upload')


def get_images_pk_from_session_or_none(request):
    if request.session.get('image_1_pk', '') and request.session.get('image_2_pk', ''):
        return [request.session.get('image_1_pk'), request.session.get('image_2_pk')]
    else:
        return None


def log_image_upload(request, image):
    try:
        image_logger.info(
            'DATE: {}; IMAGE_PRIMARY_KEY: {}; IMAGE_OWNER: {}; IP: {}; BROWSER_INFO: {}'.format(
                image.date_created,
                image.pk,
                image.owner,
                request.META.get('REMOTE_ADDR', '') or request.META.get('HTTP_X_FORWARDED_FOR', ''),
                request.META.get('HTTP_USER_AGENT')
            ))
    except:
        pass
