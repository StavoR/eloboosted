from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

from eloboosted import models as eloboosted_models
from imagepick.settings import MAX_SIZE_FOR_UPLOADED_IMAGE


def baseurl(request):
    """
    Return a BASE_URL template context for the current request.
    """
    if request.is_secure():
        scheme = 'https://'
    else:
        scheme = 'http://'

    return {'BASE_URL': scheme + request.get_host(), 'MAX_FILE_SIZE': MAX_SIZE_FOR_UPLOADED_IMAGE}


def have_18_age(request):
    rez = {
        'show_18_age_modal': ''
    }
    if not request.session.get('have_18_age', ''):
        rez['show_18_age_modal'] = True
    return rez


def show_new_images_more_often(request):
    show_new, created = eloboosted_models.Settings.objects.get_or_create(name='show_new')
    if created:
        show_new.value = 'False'
        show_new.save()
    return {'SHOW_NEW': show_new.value}


def check_for_uploaded_image(request):
    try:
        if request.user.is_authenticated() and request.session.get('uploaded_image_pk', ''):
            image_pk = request.session.pop('uploaded_image_pk')
            if request.user.get_images_count() < 3:
                image = eloboosted_models.Image.objects.get(pk=image_pk)
                image.owner = request.user
                image.save()
                messages.success(request, _('Image has been added to your profile.'))
            else:
                messages.warning(request, _('You already have 3 images.'))
    except:
        pass
    return {}
