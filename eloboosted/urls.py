from django.conf.urls import url, patterns

from . import views

urlpatterns = patterns('',
                       url(r'^$', views.ImageComparison.as_view(), name='categories_for_comparison'),
                       url(r'^comparison/(?P<category_pk>\d+)/$', views.ImageComparison.as_view(), name='comparison'),

                       url(r'^upload/$', views.UploadImage.as_view(), name='upload'),

                       url(r'^rating/$', views.CategoryListForRating.as_view(), name='categories_for_rating'),
                       url(r'^rating/(?P<category_pk>\d+)/$', views.category_sort_for_rating, name='sort_for_rating'),
                       url(r'^rating/(?P<category_pk>\d+)/(?P<filter>\w+)/$', views.Rating.as_view(), name='rating'),

                       url(r'^show_new/$', views.show_new, name='show_new'),

                       )
