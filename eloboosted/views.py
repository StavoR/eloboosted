import datetime

from django.contrib import messages
from django.core.urlresolvers import reverse
from isoweek import Week
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views.generic import View, CreateView, ListView
from django.utils.translation import ugettext as _

from . import models, forms
from . import elo_calculation
from imagepick.utils import log_image_upload


class ImageComparison(View):
    template_name = 'eloboosted/comparison.html'

    def get(self, request, *args, **kwargs):
        category_pk = self.kwargs.get('category_pk', '')

        if not category_pk:
            return render(request, self.template_name, {'category_list': models.Category.objects.all()})

        else:
            category_pk = int(category_pk)
            image_1_pk = request.session.get('image_1_pk', '')
            image_2_pk = request.session.get('image_2_pk', '')
            if not elo_calculation.check_for_category_and_owner(image_1_pk, image_2_pk, category_pk, self.request):
                images_pk_for_comparison = elo_calculation.get_images_pk_to_comparison(category_pk, request.user.pk)
                if images_pk_for_comparison:
                    image_1_pk, image_2_pk = images_pk_for_comparison
                    request.session['image_1_pk'] = image_1_pk
                    request.session['image_2_pk'] = image_2_pk
                else:
                    return HttpResponse('No moderated images in category {}'.format(
                        get_object_or_404(models.Category, pk=category_pk)))

            return render(request, self.template_name,
                          {'image_1': get_object_or_404(models.Image.moderated_objects.select_related('owner'),
                                                        pk=image_1_pk),
                           'image_2': get_object_or_404(models.Image.moderated_objects.select_related('owner'),
                                                        pk=image_2_pk)})

    def post(self, request, *args, **kwargs):
        category_pk = self.kwargs.get('category_pk', '')
        try:
            winner_pk = int(request.POST.get('image_pk', ''))
            if not winner_pk:
                raise ValueError('winner_pk is "{}"'.format(winner_pk))

            image_1_pk = request.session.pop('image_1_pk')
            image_2_pk = request.session.pop('image_2_pk')

            if winner_pk not in [image_1_pk, image_2_pk]:
                raise ValueError('winner_pk should be in request.session')

            loser_pk = image_1_pk if winner_pk == image_2_pk else image_2_pk

            elo_calculation.calculate_elo(winner_pk=winner_pk, loser_pk=loser_pk)
        except KeyError:
            pass
        except ValueError:
            pass

        return HttpResponseRedirect(reverse('eloboosted:comparison', kwargs={'category_pk': category_pk}))


class UploadImage(CreateView):
    template_name = 'eloboosted/upload.html'
    model = models.Image

    def get_form_class(self):
        if self.request.user.is_authenticated():
            return forms.ImageForm
        else:
            return forms.ImageFormAnonymousUser

    def get_context_data(self, **kwargs):
        context = super(UploadImage, self).get_context_data(**kwargs)
        if self.request.session.get('uploaded_image_pk', ''):
            context['has_uploaded_image'] = True
        return context

    def form_valid(self, form):
        if not self.request.session.get('uploaded_image_pk', ''):
            image = form.save(commit=False)
            if self.request.user.is_authenticated():
                image.owner = self.request.user
            else:
                image.owner = None
            image.save()
            log_image_upload(self.request, image)
            if image.owner is None:
                self.request.session['uploaded_image_pk'] = image.pk
                messages.success(self.request,
                                 _('Image successfully uploaded. Sign in and it will show up in your profile.'))
                return HttpResponseRedirect(reverse('account_login'))
            else:
                messages.success(self.request,
                                 _('Image successfully uploaded and sent for moderation.'))
                return HttpResponseRedirect(reverse('usermanagement:profile'))


class CategoryListForRating(ListView):
    template_name = 'eloboosted/categories_for_rating.html'
    model = models.Category


def category_sort_for_rating(request, category_pk):
    category = get_object_or_404(models.Category, pk=category_pk)
    return render(request, 'eloboosted/categories_for_rating.html', {'category_pk': category.pk})


class Rating(ListView):
    template_name = 'eloboosted/rating.html'
    model = models.Image

    def get_queryset(self):
        category_pk = int(self.kwargs.get('category_pk'))
        category = get_object_or_404(models.Category, pk=category_pk)
        multiplier = 2 if self.request.user.is_authenticated() else 1
        objects = None
        start_date = ''
        end_date = ''
        show_count = ''

        if self.kwargs.get('filter') == 'new':
            # now = datetime.date.today()
            # start_date = datetime.datetime.combine(now, datetime.time.min) - datetime.timedelta(days=1)
            # end_date = datetime.datetime.combine(now, datetime.time.max) - datetime.timedelta(days=1)
            start_date = datetime.datetime.now() - datetime.timedelta(days=5)
            end_date = datetime.datetime.now()
            # show_count = 5

        # elif self.kwargs.get('filter') == 'weekly':
        #     current_year, week_number, day_of_week = datetime.date.today().isocalendar()
        #     if day_of_week < 5:
        #         week_number -= 1
        #     start_date = Week(current_year, week_number).friday() - datetime.timedelta(days=7)
        #     end_date = Week(current_year, week_number).thursday()
        #     start_date = datetime.datetime.combine(start_date, datetime.time.min)
        #     end_date = datetime.datetime.combine(end_date, datetime.time.max)
        #     show_count = 10
        #
        # elif self.kwargs.get('filter') == 'monthly':
        #     previous = datetime.date.today().replace(day=1) - datetime.timedelta(days=1)
        #     start_date = datetime.datetime.combine(previous.replace(day=1), datetime.time.min)
        #     end_date = datetime.datetime.combine(previous, datetime.time.max)
        #     show_count = 25

        elif self.kwargs.get('filter') == 'absolute':
            show_count = 100

        if start_date and end_date:
            objects = self.model.moderated_objects.exclude(owner=None).filter(
                date_created__gte=start_date,
                date_created__lte=end_date,
                category=category
            )
        elif show_count:
            objects = self.model.moderated_objects.exclude(owner=None).filter(category=category)[
                      :show_count * multiplier]
        if objects is not None:
            return objects.select_related('owner')
        else:
            raise Http404


def show_new(request):
    if request.user.is_superuser:
        show_new_obj = models.Settings.objects.get(name='show_new')
        show_new_val = show_new_obj.value
        if show_new_val == 'False':
            show_new_obj.value = 'True'
        elif show_new_val == 'True':
            show_new_obj.value = 'False'
        show_new_obj.save()
        return HttpResponseRedirect(reverse('admin:index'))
