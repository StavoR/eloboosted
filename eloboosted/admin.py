from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext_lazy as _

from . import models


class EloInline(admin.TabularInline):
    model = models.Elo
    readonly_fields = ('amount', 'date')
    extra = 0
    can_delete = False


class NullFilterSpec(SimpleListFilter):
    title = _('Owners')

    parameter_name = 'owner'

    def lookups(self, request, model_admin):
        return (
            ('1', _('Has owner')),
            ('0', _('Has no owner')),
        )

    def queryset(self, request, queryset):
        kwargs = {
            '%s' % self.parameter_name: None,
        }
        if self.value() == '0':
            return queryset.filter(**kwargs)
        if self.value() == '1':
            return queryset.exclude(**kwargs)
        return queryset


@admin.register(models.Image)
class ImageAdmin(admin.ModelAdmin):
    def get_actions(self, request):
        actions = super(ImageAdmin, self).get_actions(request)
        if request.user.groups.filter(name='moderators'):
            del actions['delete_selected']
        return actions

    def get_readonly_fields(self, request, *args, **kwargs):
        if request.user.groups.filter(name='moderators'):
            return 'image', 'category', 'owner', 'current_elo', 'current_elo_date'
        return super(ImageAdmin, self).get_readonly_fields(request, *args, **kwargs)

    list_display = (
        'admin_image', 'moderation_passed', 'owner', 'category', 'current_elo', 'current_elo_date', 'date_created')
    date_hierarchy = 'date_created'
    list_editable = ('moderation_passed',)
    list_filter = ('moderation_passed', 'category', 'date_created', NullFilterSpec)
    ordering = ('-date_created',)
    search_fields = ('owner__username',)
    view_on_site = False

    actions = ['passed_image', 'unpassed_image', 'remove_owner_image']

    def passed_image(modeladmin, request, queryset):
        queryset.update(moderation_passed=True)

    passed_image.short_description = _('Moderate selected images')

    def unpassed_image(modeladmin, request, queryset):
        queryset.update(moderation_passed=False)

    unpassed_image.short_description = _('Undo moderation on selected images')

    def remove_owner_image(modeladmin, request, queryset):
        queryset.update(owner=None)

    remove_owner_image.short_description = _('Remove ownership from selected images')

    inlines = [
        EloInline,
    ]


@admin.register(models.Elo)
class EloAdmin(admin.ModelAdmin):
    list_display = ('image', '__str__', 'date',)


@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    pass
