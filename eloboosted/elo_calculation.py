import datetime
from django.shortcuts import get_object_or_404
from django.db.models import Func, F
from random import randint

from . import models


def get_images_pk_to_comparison(category_pk, user_pk):
    try:
        objects = models.Image.moderated_objects.exclude(owner=None).filter(category__pk=category_pk)

        if user_pk is not None:
            objects = objects.exclude(owner__pk=user_pk)

        show_new_val = models.Settings.objects.get(name='show_new').value

        if show_new_val == 'True' and randint(0, 2) == 1:
            start_date = datetime.datetime.now() - datetime.timedelta(days=5)
            end_date = datetime.datetime.now()
            image_1 = objects.filter(
                date_created__gte=start_date,
                date_created__lte=end_date,
            ).order_by('?').first()
            if image_1:
                image_2 = objects.exclude(pk=image_1.pk).filter(
                    date_created__gte=start_date,
                    date_created__lte=end_date,
                    current_elo__gte=image_1.current_elo - 200,
                    current_elo__lte=image_1.current_elo + 200
                ).order_by('?').first()
            else:
                image_2 = None
        else:
            image_1 = objects.order_by('?').first()
            image_2 = objects.exclude(pk=image_1.pk).filter(
                current_elo__gte=image_1.current_elo - 200,
                current_elo__lte=image_1.current_elo + 200
            ).order_by('?').first()
        if not image_1:
            image_1 = objects.order_by('?').first()
        if not image_2:
            image_2 = objects.exclude(pk=image_1.pk).annotate(
                abs_diff=Func(F('current_elo') - image_1.current_elo, function='ABS')).order_by('abs_diff').first()
        return image_1.pk, image_2.pk
    except:
        return False


def calculate_elo(winner_pk, loser_pk):
    loser = get_object_or_404(models.Image.moderated_objects, pk=loser_pk)
    winner = get_object_or_404(models.Image.moderated_objects, pk=winner_pk)

    r_winner = winner.current_elo
    r_loser = loser.current_elo

    k_winner = winner.get_elo_coefficient()
    k_loser = loser.get_elo_coefficient()

    e_winner = 1.0 / (1.0 + 10.0 ** ((r_loser - r_winner) / 400.0))
    e_loser = 1.0 / (1.0 + 10.0 ** ((r_winner - r_loser) / 400.0))

    new_r_winner = r_winner + k_winner * (1.0 - e_winner)
    new_r_loser = r_loser + k_loser * (0.0 - e_loser)

    loser.elo_set.create(amount=new_r_loser)
    winner.elo_set.create(amount=new_r_winner)


def check_for_category_and_owner(image_1_pk, image_2_pk, category_pk, request):
    if not image_1_pk or not image_2_pk:
        return False
    category_list = models.Image.moderated_objects.filter(pk__in=[image_1_pk, image_2_pk]).values_list('category__pk',
                                                                                                       flat=True)
    try:
        image_1 = get_object_or_404(models.Image.moderated_objects, pk=image_1_pk)
        image_2 = get_object_or_404(models.Image.moderated_objects, pk=image_2_pk)
        if request.user == image_1.owner or request.user == image_2.owner:
            return False
        if category_list[0] == category_list[1] and category_pk in category_list:
            return True
        else:
            return False
    except:
        return False
