from django import template

register = template.Library()


@register.assignment_tag
def get_bootstrap_alert_msg_css_name(tags):
    return 'danger' if tags == 'error' else tags


@register.simple_tag
def active_page(request, view_names):
    from django.core.urlresolvers import resolve, Resolver404
    if not request:
        return ''
    try:
        for view_name in view_names.split(', '):
            if resolve(request.path_info).url_name == view_name:
                return 'active'
        return ''
    except Resolver404:
        return ''


@register.simple_tag
def url_replace(request, field, value):
    dict_ = request.GET.copy()

    if not value:
        dict_.pop(field, None)
    else:
        dict_[field] = value

    if len(dict_):
        return u'?{}'.format(dict_.urlencode())
    else:
        return request.META.get('PATH_INFO')
