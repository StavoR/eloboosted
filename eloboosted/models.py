import datetime
import os
import uuid

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.html import format_html
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFit, Transpose
from django.utils.translation import ugettext_lazy as _

from .managers import ImageManager


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('uploaded_images/{}/{}'.format(
        datetime.date.today().year,
        datetime.date.today().month
    ), filename)


class Category(models.Model):
    name = models.CharField(_('Name'), max_length=100)

    def __str__(self):
        return str(self.name)

    def clean(self):
        if not self.pk:
            if Category.objects.all().count() >= 2:
                raise ValidationError(_('You can\'t create more than 2 categories.'))

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')


class Image(models.Model):
    image = models.ImageField(_('Image'),
                              upload_to=get_file_path)  # TODO: clear CACHE (ask Glebaysan about moderators and admins)
    compressed_image = ImageSpecField(source='image',
                                      processors=[Transpose(), ResizeToFit(900, 600)],
                                      format='JPEG',
                                      options={'quality': 80})
    category = models.ForeignKey(Category, verbose_name=_('Category'))
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              verbose_name=_('Owner'),
                              null=True, blank=True)
    date_created = models.DateTimeField(_('Upload date'), auto_now_add=True)
    current_elo = models.FloatField(_('Current elo'), null=True)
    current_elo_date = models.DateTimeField(_('Latest changing elo date'), null=True)
    moderation_passed = models.BooleanField(_('Moderated'), default=False)

    objects = models.Manager()
    moderated_objects = ImageManager()

    def __str__(self):
        return str(self.date_created)

    def admin_image(self):
        view_original = _('View original')
        try:
            return format_html('<img src="{}" width=100> <a href="{}" target="_blank">{}</a>',
                               self.compressed_image.url,
                               self.image.url,
                               view_original)
        except FileNotFoundError:
            return format_html('<img src="{}" width=100> <a href="{}" target="_blank">{}</a>',
                               self.image.url,
                               self.image.url,
                               view_original)

    admin_image.allow_tags = True
    admin_image.short_description = _('Image')

    def get_absolute_url(self):
        return reverse('eloboosted:comparison', kwargs={'category_pk': self.category.pk})

    def get_elo_count(self):
        return self.elo_set.all().count()

    def get_elo_coefficient(self):
        rez = 20.0
        if self.get_elo_count() <= 30:
            rez = 40.0
        elif self.current_elo >= 2400.0:
            rez = 10.0
        return rez

    def save(self, *args, **kwargs):
        if self.owner is not None:
            if not self.pk and self.owner.get_images_count() < 3:
                super(Image, self).save(*args, **kwargs)
                self.elo_set.create(amount=600)
            elif self.pk and self.owner.get_images_count() <= 3:
                super(Image, self).save(*args, **kwargs)
            else:
                raise ValidationError('user can\'t upload more than 3 images')
        else:
            if not self.pk:
                super(Image, self).save(*args, **kwargs)
                self.elo_set.create(amount=600)
            else:
                super(Image, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('Image')
        verbose_name_plural = _('Images')
        ordering = ('-current_elo',)


class Elo(models.Model):
    image = models.ForeignKey(Image, verbose_name=_('Image'))
    amount = models.FloatField(_('Amount'))
    date = models.DateTimeField(_('Date'), auto_now_add=True)

    def __str__(self):
        return str(self.amount)

    def save(self, *args, **kwargs):
        super(Elo, self).save(*args, **kwargs)
        self.image.current_elo = self.amount
        self.image.current_elo_date = self.date
        self.image.save()

    class Meta:
        verbose_name = _('Elo')
        verbose_name_plural = _('Elo')
        ordering = ['-date']


class Settings(models.Model):
    name = models.CharField(max_length=100)
    value = models.CharField(max_length=100)
