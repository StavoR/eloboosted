from django import forms
from django.core.files.images import get_image_dimensions
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from . import models


class ImageForm(forms.ModelForm):
    def clean_image(self):
        image = self.cleaned_data.get('image', False)
        if image:

            if image._size > settings.MAX_SIZE_FOR_UPLOADED_IMAGE * 1024 * 1024:
                raise forms.ValidationError(
                    _("Image file size must be lower than {size} MB.").format(
                        size=settings.MAX_SIZE_FOR_UPLOADED_IMAGE))

            w, h = get_image_dimensions(image)
            if w * h > settings.PIXELS_LIMIT:
                raise forms.ValidationError(_("Image dimensions are too large."))

            return image
        else:
            raise forms.ValidationError(_('Something went wrong. Try again later.'))

    class Meta:
        model = models.Image
        fields = [
            'image',
            'category'
        ]


class ImageFormAnonymousUser(ImageForm):
    rules = forms.BooleanField(
        initial=False,
        error_messages={'required': _('You have to agree the rules.')},
    )
