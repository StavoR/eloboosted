import os
import uuid

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core import validators
from django.core.validators import MinValueValidator
from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFit, Transpose
from paypal.standard.ipn.models import PayPalIPN
from django.utils.translation import ugettext_lazy as _

from eloboosted import models as eloboosted_models


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('images_attached_to_messages/{}'.format(instance.sender), filename)


def set_default_country():
    try:
        return Country.objects.get(iso='RU').pk
    except:
        return None


class MyUser(AbstractUser):
    SEX_CHOISES = (('M', _('Male')), ('F', _('Female')))
    sex = models.CharField(_('Sex'), choices=SEX_CHOISES, max_length=6)
    age = models.PositiveIntegerField(_('Age'), validators=[MinValueValidator(18)])
    country = models.ForeignKey('Country', verbose_name=_('Country'))
    favorite_images = models.ManyToManyField(eloboosted_models.Image, blank=True)
    likes_balance = models.PositiveIntegerField(_('Balance'), default=0)
    allow_messages = models.BooleanField(_('Messages privacy'), default=True,
                                         help_text=_('Allow users to send you messages.'))
    allow_see_all_images = models.BooleanField(
        _('Images privacy'),
        default=True,
        help_text=_('Allow users to view your images.')
    )
    REQUIRED_FIELDS = ['email', 'age', 'country']

    def get_unread_messages_count(self):
        return self.received_messages.filter(is_read=False).count()

    def get_images_count(self):
        return self.image_set.all().count()

    def get_favorite_images(self):
        return self.favorite_images.filter(moderation_passed=True)

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')


class Country(models.Model):
    name = models.CharField(_('Name'), max_length=1000)
    iso = models.CharField('ISO', max_length=2, validators=[
        validators.RegexValidator(r'^[A-Z]{2}$',
                                  _('Wrong ISO code'),
                                  'invalid'),
    ])

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')


class Message(models.Model):
    sender = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='sent_messages', verbose_name=_('Sender'))
    receiver = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='received_messages', verbose_name=_('Receiver'))
    attached_image = ProcessedImageField(upload_to=get_file_path,
                                         processors=[Transpose(), ResizeToFit(500, 400)],
                                         format='JPEG',
                                         options={'quality': 80},
                                         null=True, blank=True, verbose_name=_('Attached image'))
    text = models.TextField(_('Text'), max_length=1000, blank=True, validators=[
        validators.RegexValidator(r'.*\S.*',
                                  _('This field is required.'),
                                  'invalid'),
    ])
    date_sent = models.DateTimeField(_('Sending date'), auto_now_add=True)
    is_read = models.BooleanField(_('Is read'), default=False)

    def __str__(self):
        return _('From {sender} to {receiver}').format(sender=self.sender, receiver=self.receiver)

    class Meta:
        verbose_name = _('Message')
        verbose_name_plural = _('Messages')
        ordering = ('-date_sent',)


class Likes(models.Model):
    amount = models.PositiveIntegerField(_('Amount'), validators=[MinValueValidator(1)])
    date = models.DateTimeField(_('Date'), auto_now_add=True, editable=True)

    class Meta:
        abstract = True


class LikesTransfer(Likes):
    from_user = models.ForeignKey(MyUser, related_name='sent_likes', verbose_name=_('From user'))
    to_user = models.ForeignKey(MyUser, related_name='received_likes', verbose_name=_('To user'))

    def __str__(self):
        if self.from_user:
            return _('From {sender} to {receiver}').format(sender=self.from_user, receiver=self.to_user)

    def save(self, *args, **kwargs):
        if not self.pk:
            super(LikesTransfer, self).save(*args, **kwargs)
            self.from_user.likes_balance -= self.amount
            self.to_user.likes_balance += self.amount
            self.from_user.save()
            self.to_user.save()

    class Meta:
        verbose_name = _('Likes transfer')
        verbose_name_plural = _('Likes transfers')
        ordering = ('-date',)


class LikesBuying(Likes):
    payment = models.OneToOneField(PayPalIPN, verbose_name='PayPal IPN')
    user = models.ForeignKey(MyUser, verbose_name=_('User'))

    def __str__(self):
        return str(self.payment)

    def save(self, *args, **kwargs):
        if not self.pk:
            super(LikesBuying, self).save(*args, **kwargs)
            self.user.likes_balance += self.amount
            self.user.save()

    class Meta:
        verbose_name = _('Likes buying')
        verbose_name_plural = _('Likes buyings')
        ordering = ('-date',)


class LikesOut(Likes):
    user = models.ForeignKey(MyUser, verbose_name=_('User'))

# class OutStatus(models.Model):
#     name = models.CharField('Название', max_length=20)
#
#     def __str__(self):
#         return str(self.name)
#
#     class Meta:
#         verbose_name = 'Статус операции вывода'
#         verbose_name_plural = 'Статусы операции вывода'
#
#
# class GiveUserTheMoney(models.Model):
#     user = models.ForeignKey(MyUser, verbose_name='Пользователь')
#     name = models.CharField('Имя', max_length=60, help_text='Имя с банковской карты')
#     number = models.CharField('Номер карты', max_length=16, help_text='Без пробелов или тире')
#     date = models.CharField('Дата окончания действия карты', max_length=7, help_text='В формате "ММ/ГГГГ"')
#     cvc = models.CharField('Защитный код', max_length=3)
#     amount = models.PositiveIntegerField('Количество', )
#
#     Not available for users - those settings is under admins jurisdiction
#     date_of_operation = models.DateTimeField('Дата операции', auto_now_add=True, editable=True)
#     status = models.ForeignKey(OutStatus, verbose_name='Статус операции вывода')
#     additional_status_field = models.CharField('Дополнительная информация о статусе', max_length=100,
#                                                help_text='Например, в случае отказа', blank=True)
#
#     def __str__(self):
#         return str(self.user)
#
#     class Meta:
#         verbose_name = 'Операция вывода'
#         verbose_name_plural = 'Операции вывода'
