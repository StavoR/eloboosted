from django import forms
from django.contrib.auth import get_user_model
from django.core.files.images import get_image_dimensions
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from . import models


class SignupForm(forms.ModelForm):
    rules = forms.BooleanField(
        initial=False,
        error_messages={'required': _('You have to agree the rules.')},
    )

    class Meta:
        model = get_user_model()
        fields = ['sex', 'age', 'country']

    def signup(self, request, user):
        user.save()


class MessageForm(forms.ModelForm):
    def clean_attached_image(self):
        image = self.cleaned_data.get('attached_image', False)
        if image:

            if image._size > settings.MAX_SIZE_FOR_UPLOADED_IMAGE * 1024 * 1024:
                raise forms.ValidationError(
                    _("Image file size must be lower than {size} MB.").format(size=settings.MAX_SIZE_FOR_UPLOADED_IMAGE))

            w, h = get_image_dimensions(image)
            if w * h > settings.PIXELS_LIMIT:
                raise forms.ValidationError(_("Image dimensions are too large."))

            return image

    def clean(self):
        cleaned_data = super(MessageForm, self).clean()
        image = cleaned_data.get('attached_image', '')
        text = cleaned_data.get('text', '')
        if not image and not text:
            raise forms.ValidationError(_('Enter a message and/or upload an image.'))

    class Meta:
        model = models.Message
        fields = ['text', 'attached_image']

#
# class GiveUserTheMoneyForm(forms.ModelForm):
#     class Meta:
#         model = models.GiveUserTheMoney
#         fields = [
#             'name',
#             'number',
#             'date',
#             'cvc',
#             'amount'
#         ]
