from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from eloboosted import models as eloboosted_models
from . import models


class ImageInline(admin.TabularInline):
    model = eloboosted_models.Image
    extra = 0


@admin.register(models.MyUser)
class MyUserAdmin(UserAdmin):
    list_display = UserAdmin.list_display + ('sex', 'age', 'country')
    fieldsets = (
        (None, {
            'fields': ('username', 'password')}),
        (_('Personal info'), {
            'classes': ('collapse',),
            'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {
            'classes': ('collapse',),
            'fields': ('is_active', 'is_staff', 'is_superuser',
                       'groups', 'user_permissions')}),
        (_('Important dates'), {
            'classes': ('collapse',),
            'fields': ('last_login', 'date_joined')}),
        (_('Misc'), {
            'classes': ('collapse',),
            'fields': (
                'sex', 'age', 'country', 'favorite_images', 'allow_messages', 'allow_see_all_images', 'likes_balance'),

        })
    )
    search_fields = ('username',)
    filter_horizontal = UserAdmin.filter_horizontal + ('favorite_images',)
    inlines = [
        ImageInline,
    ]


@admin.register(models.Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'text', 'date_sent', 'is_read')


@admin.register(models.Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ('name_ru', 'name_en', 'iso')


@admin.register(models.LikesTransfer)
class LikesTransferAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'amount', 'date',)


@admin.register(models.LikesBuying)
class LikesTransferAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'amount', 'date',)


@admin.register(models.LikesOut)
class LikesTransferAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'amount', 'date',)
