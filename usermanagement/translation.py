from modeltranslation.translator import TranslationOptions, register

from .models import Country


@register(Country)
class CountryTranslationOptions(TranslationOptions):
    fields = ('name',)
