from django import template
from django.db.models import Q

from usermanagement import models

register = template.Library()


@register.filter
def unread_msg_count(sender, request):
    try:
        sender_pk = sender.pk
        return models.Message.objects.filter(
            Q(sender__pk=sender_pk) & Q(receiver__pk=request.user.pk) & Q(is_read=False)).count()
    except:
        return 0
