from django.conf.urls import url, patterns, include
from django.contrib.auth.decorators import login_required

from . import views

urlpatterns = patterns('',
                       url(r'^have-18-age/$', views.have_18_age, name='have_18_age'),

                       url(r'^images/$', views.ViewAllImagesOfUser.as_view(), name='images_of_user'),  # AJAX!

                       url(r'^send-message/$', login_required(views.send_message), name='send_message'),  # AJAX!

                       url(r'^check-on-login/$', login_required(views.check_on_login), name='check_on_login'),

                       url(r'^check-on-logout/$', views.check_on_logout, name='check_on_logout'),

                       url(r'^paypal/', include('paypal.standard.ipn.urls')),

                       url(r'^profile/$', login_required(views.Profile.as_view()), name='profile'),

                       url(r'^likes/$', login_required(views.LikesPage.as_view()), name='likes'),

                       url(r'^transfer-likes/$', login_required(views.transfer_likes), name='transfer_likes'),  # AJAX!

                       url(r'^upload/$', login_required(views.UploadImageFromProfile.as_view()), name='upload'),

                       url(r'^chat/$', login_required(views.ChatPage.as_view()), name='chat_page'),

                       url(r'^chat/(?P<user_pk>\d+)/$', login_required(views.Chat.as_view()), name='chat'),

                       url(r'^delete/(?P<pk>\d+)/$', login_required(views.delete_image), name='delete'),

                       url(r'^change-privacy-settings/$', login_required(views.privacy_settings),
                           name='change_privacy_settings'),

                       url(r'^favorites/$', login_required(views.FavoriteImages.as_view()), name='favorites'),

                       url(r'^favorite/add/$', login_required(views.toggle_favorite), {'operation': 'add'},
                           name='add_to_favorite'),  # AJAX!

                       url(r'^favorite/remove/$', login_required(views.toggle_favorite), {'operation': 'remove'},
                           name='remove_to_favorite'
                           ),  # AJAX!

                       )
