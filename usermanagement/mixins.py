from . import models


class ChatUsersMixin(object):
    def get_context_data(self, **kwargs):
        context = super(ChatUsersMixin, self).get_context_data(**kwargs)

        senders_pk_list = self.request.user.received_messages.all().values_list('sender', flat=True)
        senders_pk_list = list(set(senders_pk_list))

        receivers_pk_list = self.request.user.sent_messages.all().values_list('receiver', flat=True)
        receivers_pk_list = list(set(receivers_pk_list))

        chat_users_pk_list = list(set(senders_pk_list + receivers_pk_list))

        chat_users = models.MyUser.objects.filter(pk__in=chat_users_pk_list).exclude(pk=self.request.user.pk)
        context['chat_users'] = chat_users
        return context
