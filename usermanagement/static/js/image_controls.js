var send_message_url = 'user/send-message/';
var show_images_of_user_url = 'user/images/';
var toggle_favorite_url = 'user/favorite/';
var transfer_likes_url = 'user/transfer-likes/';

function send_message(element) {
    var $element = $(element);
    $element.button('loading');
    var form = $('#message_data').get(0);
    var form_data = new FormData(form);
    var $message_text_elem = $('#message_textarea');
    var $response_block = $('#message_response');
    var $file = $('#message_attached_image');
    $response_block.fadeOut(0);

    $.ajax({
        url: BASE_URL + send_message_url,
        type: "POST",
        data: form_data,
        cache: false,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function (data) {
            if (data['status'] == 'success') {
                $response_block.fadeIn(0);
                $response_block.html('<div class="alert alert-success">' + data['message'] + '</div>');
                $response_block.fadeOut(2000);
                $message_text_elem.val('');
                $file.fileinput('clear')
            }
            if (data['status'] == 'error') {
                $response_block.fadeIn(0);
                $response_block.html('<div class="alert alert-danger">' + data['message'] + '</div>');
            }
            $element.button('reset');
        },
        error: function (jqXHR, textStatus, errorMessage) {
            console.log(errorMessage);
        }
    });
}

function show_images_of_user(image_pk) {
    $.get(
        BASE_URL + show_images_of_user_url,
        {
            'image_pk': image_pk
        },
        function (data) {
            $('#images_of_user_modal_content').html(data);
            $('#images_of_user_modal').modal({'show': true});
        },
        'html'
    )
}

function toggle_favorite(element, image_pk) {
    var $element = $(element);
    var operation = $element.attr('operation');
    var full_url = BASE_URL + toggle_favorite_url + operation + '/';
    $.post(
        full_url,
        {
            'image_pk': image_pk,
            'csrfmiddlewaretoken': csrftoken
        },
        function (data) {
            if (data['status'] == 'success') {
                if (operation == 'add') {
                    $element.attr('operation', 'remove').addClass('ico-favor-on');
                }
                if (operation == 'remove') {
                    $element.attr('operation', 'add').removeClass('ico-favor-on');
                }
            }
        }
        ,
        'json'
    )
}

function show_likes_modal(image_pk, image_owner) {
    $('#transfer_likes_modal').modal('show');
    $('#image_pk_for_likes').val(image_pk);
    $('#likes_to_user').html(image_owner);
    $('#likes_amount').val(1);
}

function show_message_modal(image_pk, image_owner) {
    $('#send_message_modal').modal('show');
    $('#image_pk_for_message').val(image_pk);
    $('#message_to_user').html(image_owner);
}

function transfer_likes(element) {
    var $element = $(element);
    $element.button('loading');
    var image_pk = $('#image_pk_for_likes').val();
    var $likes_balance = $('#likes_balance');
    var likes_balance_val = parseInt($likes_balance.html());
    var $likes_response = $('#likes_response');
    var likes_amount = $('#likes_amount').val();
    $likes_response.fadeOut(0);
    $.post(
        BASE_URL + transfer_likes_url,
        {
            'image_pk': image_pk,
            'amount': likes_amount,
            'csrfmiddlewaretoken': csrftoken
        },
        function (data) {
            if (data['status'] == 'success') {
                $likes_balance.html(likes_balance_val - likes_amount);
                $('#likes_amount').val(1);
                $likes_response.fadeIn(0);
                $likes_response.html('<div class="alert alert-success">' + data['message'] + '</div>');
                $likes_response.fadeOut(2000);
            }
            if (data['status'] == 'error') {
                $('#likes_amount').val(1);
                $likes_response.fadeIn(0);
                $likes_response.html('<div class="alert alert-danger">' + data['message'] + (likes_balance_val + 1) + '</div>');
            }
            $element.button('reset');
        },
        'json'
    )
}