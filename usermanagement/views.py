import json

from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, TemplateView, CreateView
from paypal.standard.ipn.signals import valid_ipn_received
from paypal.standard.models import ST_PP_COMPLETED
from django.utils.translation import ugettext as _

from eloboosted import models as eloboosted_models
from eloboosted import forms as eloboosted_forms
from . import models
from .forms import MessageForm
from imagepick.utils import log_image_upload
from .mixins import ChatUsersMixin

FULL_SITE = 'http://' + get_current_site(None).domain


def have_18_age(request):
    request.session['have_18_age'] = 'True'
    messages.info(request,
                  _('Using this website, you agree to the use cookies to improve the level of service.'))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class ViewAllImagesOfUser(ListView):
    template_name = 'usermanagement/ajax/images_of_user.html'
    model = eloboosted_models.Image

    def get_queryset(self):
        image = get_object_or_404(self.model.moderated_objects, pk=self.request.GET.get('image_pk'))
        owner = image.owner
        if owner.allow_see_all_images:
            return self.model.moderated_objects.filter(owner=owner).select_related('owner')
        else:
            raise Http404


def send_message(request):
    if request.is_ajax():
        try:
            form = MessageForm(request.POST, request.FILES)
            if form.is_valid():
                image_pk = int(request.POST.get('image_pk'))
                image = get_object_or_404(eloboosted_models.Image.moderated_objects, pk=image_pk)
                owner = image.owner
                full_form = form.save(commit=False)
                full_form.sender = request.user
                full_form.receiver = owner
                full_form.save()
                status = 'success'
                message = _('Message successfully sent!')
            else:
                status = 'error'
                message = _('Enter message text.')
        except:
            status = 'error'
            message = _('Enter a message and/or upload an image.')

        return HttpResponse(json.dumps({
            'message': message,
            'status': status
        }))


class Profile(TemplateView):
    template_name = 'usermanagement/profile.html'

    def get_context_data(self, **kwargs):
        context = super(Profile, self).get_context_data(**kwargs)
        context['upload_form'] = eloboosted_forms.ImageForm
        return context


class ChatPage(ChatUsersMixin, TemplateView):
    template_name = 'usermanagement/chat.html'


class UploadImageFromProfile(CreateView):
    template_name = 'usermanagement/profile.html'
    form_class = eloboosted_forms.ImageForm

    def get_context_data(self, **kwargs):
        context = super(UploadImageFromProfile, self).get_context_data(**kwargs)
        context['upload_form'] = context.pop('form')
        return context

    def form_valid(self, form):
        image = form.save(commit=False)
        image.owner = self.request.user
        image.save()
        log_image_upload(self.request, image)
        messages.success(self.request, _('Image successfully uploaded and sent for moderation.'))
        return HttpResponseRedirect(reverse('usermanagement:profile'))

    def form_invalid(self, form):
        context = self.get_context_data(form=form)
        context['upload_form_invalid'] = True
        return self.render_to_response(context)


def delete_image(request, pk):
    image = get_object_or_404(eloboosted_models.Image, pk=pk)
    if image not in request.user.image_set.all():
        return HttpResponse('it\'s not your image!')
    image.owner = None
    image.save()
    messages.success(request, _('Image successfully removed.'))
    return HttpResponseRedirect(reverse('usermanagement:profile'))


def privacy_settings(request):
    allow_messages = False
    allow_see_all_images = False
    has_changed = False
    if request.POST.get('allow_messages', ''):
        allow_messages = True
    if request.POST.get('allow_see_all_images', ''):
        allow_see_all_images = True
    if allow_messages != request.user.allow_messages:
        has_changed = True
        request.user.allow_messages = allow_messages
    if allow_see_all_images != request.user.allow_see_all_images:
        has_changed = True
        request.user.allow_see_all_images = allow_see_all_images
    if has_changed:
        request.user.save()
    messages.success(request, _('Privacy settings saved.'))
    return HttpResponseRedirect(reverse('usermanagement:profile'))


class FavoriteImages(TemplateView):
    template_name = 'usermanagement/favorites.html'


def toggle_favorite(request, operation):
    if request.is_ajax():
        image_pk = request.POST.get('image_pk', '')
        if image_pk:
            image = get_object_or_404(eloboosted_models.Image.moderated_objects, pk=image_pk)
            if image.owner != request.user:
                status = 'error'
                if operation == 'add':
                    if image not in request.user.favorite_images.all():
                        request.user.favorite_images.add(image)
                        status = 'success'
                elif operation == 'remove':
                    if image in request.user.favorite_images.all():
                        request.user.favorite_images.remove(image)
                        status = 'success'
                return HttpResponse(json.dumps({'status': status}))


class Chat(ChatUsersMixin, ListView):
    template_name = 'usermanagement/chat.html'
    model = models.Message
    context_object_name = 'chat_messages'
    companion = None
    form = MessageForm

    def get_context_data(self, **kwargs):
        context = super(Chat, self).get_context_data(**kwargs)
        context['companion'] = self.companion
        if 'form' in kwargs:
            context['form'] = kwargs['form']
        else:
            context['form'] = self.form
        return context

    def post(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        form = self.form(request.POST, request.FILES)
        if form.is_valid():
            msg = form.save(commit=False)
            msg.sender = self.request.user
            msg.receiver = get_object_or_404(models.MyUser, pk=self.kwargs.get('user_pk'))
            msg.save()
            return HttpResponseRedirect(reverse('usermanagement:chat', kwargs={'user_pk': self.kwargs.get('user_pk')}))
        else:
            context = self.get_context_data(form=form)
            return self.render_to_response(context)

    def get_queryset(self):
        companion = get_object_or_404(models.MyUser, pk=self.kwargs.get('user_pk'))
        user = self.request.user

        conversation_messages = self.model.objects.filter(
            (Q(sender=user) & Q(receiver=companion)) | (Q(sender=companion) & Q(receiver=user)))

        if not conversation_messages or companion == user:
            raise Http404
        self.companion = companion
        self.model.objects.filter(Q(sender=companion) & Q(receiver=user) & Q(is_read=False)).update(is_read=True)
        return conversation_messages


def transfer_likes(request):
    if request.is_ajax():
        try:
            amount = int(request.POST.get('amount'))
            if amount <= 0:
                raise ValueError('amount must be positive!')
            if amount > request.user.likes_balance:
                raise ValueError('can\'t donate more than you\'ve got!')
            image = get_object_or_404(eloboosted_models.Image.moderated_objects, pk=request.POST.get('image_pk'))
            owner = image.owner
            models.LikesTransfer.objects.create(
                from_user=request.user,
                to_user=owner,
                amount=amount
            )
            status = 'success'
            message = _('Success!')
        except Exception as e:
            message = _('Amount of likes must be greater than 0 and lower than ')
            status = 'error'

        return HttpResponse(json.dumps({'status': status, 'message': message,
                                        }))


def check_on_login(request):
    request.session.pop('image_1_pk', None)
    request.session.pop('image_2_pk', None)
    return HttpResponseRedirect(reverse('usermanagement:profile'))


def check_on_logout(request):
    request.session['have_18_age'] = 'True'
    return HttpResponseRedirect(reverse('account_login'))


class LikesPage(TemplateView):
    template_name = 'usermanagement/likes.html'


def paypal_callback_success(sender, **kwargs):
    ipn_obj = sender
    if ipn_obj.payment_status == ST_PP_COMPLETED:
        user_pk = int(ipn_obj.custom)
        user = models.MyUser.objects.get(pk=user_pk)
        amount = ipn_obj.quantity
        models.LikesBuying.objects.create(
            payment=ipn_obj,
            user=user,
            amount=amount
        )


valid_ipn_received.connect(paypal_callback_success)
